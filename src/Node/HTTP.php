<?php
use Psr\Http\Message\ServerRequestInterface;
use React\EventLoop\Factory;
use React\Http\Response;
use React\Http\Server;
require __DIR__ . '/vendor/autoload.php';
$loop = Factory::create();
// var http = require("http");

$exports['createServer'] = function ($handleRequest) {
  return function () use (&$handleRequest) {
    return new Server($handleRequest);
  };
};

$exports['listenImpl'] = function ($server) {
  return function ($port) use (&$server) {
    return function ($hostname) use (&$port, &$server){
      return function ($backlog) use (&$hostname, &$port, &$server) {
        return function ($done) use (&$backlog, &$hostname, &$port, &$server) {
          return function () use (&$done, &$backlog, &$hostname, &$port, &$server) {
            $socket = new \React\Socket\Server($hostname . ":" . $port, $loop);
            $server->listen($socket);
            // TODO backlog stuff
            // if (backlog !== null) {
            //   server.listen(port, hostname, backlog, done);
            // } else {
            //   server.listen(port, hostname, done);
            // }
          };
        };
      };
    };
  };
};

// exports.closeImpl = function (server) {
//   return function (done) {
//     return function () {
//       server.close(done);
//     };
//   };
// };

// exports.listenSocket = function (server) {
//   return function (path) {
//     return function (done) {
//       return function () {
//         server.listen(path, done);
//       };
//     };
//   };
// };

// exports.setHeader = function (res) {
//   return function (key) {
//     return function (value) {
//       return function () {
//         res.setHeader(key, value);
//       };
//     };
//   };
// };

// exports.setHeaders = function (res) {
//   return function (key) {
//     return function (values) {
//       return function () {
//         res.setHeader(key, values);
//       };
//     };
//   };
// };

// exports.setStatusCode = function (res) {
//   return function (code) {
//     return function () {
//       res.statusCode = code;
//     };
//   };
// };

// exports.setStatusMessage = function (res) {
//   return function (message) {
//     return function () {
//       res.statusMessage = message;
//     };
//   };
// };
